### PROJET PART 4

## Structure du projet

__NB: les scripts utilise jq__
Pensez à installer jq avec : 
```console
foo@bar:~$ apt-get install jq
``` 

## HOW TO
1. __Décompresser l'archive__

2. __Initialiser le projet :__
```console
foo@bar:~$ pip install -r requirements.txt
``` 

3. __Lancer le container EC__
```console
foo@bar:~$ docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.10.1
``` 
4. __Importer les données :__ 
> depuis le répertoire /ec/
```console
foo@bar:~/ec/$ chmod +x insert_data.py
foo@bar:~/ec/$ ./insert_data.py
``` 

5. __Démarrer FastAPI__
```console
foo@bar:~/fastapi/$ uvicorn pokedex_api:api --reload & 
``` 

Vous pouvez tester l'API depuis l'adresse : http://localhost:8000/docs



## Details

__/scrapper/__

Ce repertoire continent l'outil de scraping.
Le scraping s'effectue sur le site :  "https://eternia.fr/fr/pokedex/liste-pokemon/"   
Afin d'optimiser le code le scraper utilise la class :  PokedexScraper
créée spécialement pour ce site internet.

Principal objectif du scraper développé : 
- mettre en pratique Selenium
- créer une base FR de pokedex

```console
foo@bar:~/scraper/$ pyhton3 scraper.py [--scraplimit] --ofile "mydb.csv" > data/logfile.txt
``` 
--scraplimit : permet d'effectuer des tests et de limiter le scraper à N pages
--ofile : permet de définir le nom de fichier à sauver, celui ci sera sauvé dans /data/monfichier.csv



__/data/__
Contient le fichier résultat du scraping ainsi que le fichier log correspondant


__/ec/__
Il s'agit du répertoire permettant d'intégrer les données scrapées dans une base de données ELASTIC SEARCH

Pour lancer EC de façon individuelle : 
```console
foo@bar:~$ docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.10.1
``` 


Le script d'import doit être en execution : 
```console
foo@bar:~/ec/$ chmod +x insert_data.py
foo@bar:~/ec/$ ./insert_data.py
``` 
Les données sont intégrées à EC via un pipeline puis sont traitées via mapping + analyzer

Le choix d'EC permet de répondre aux problèmatiques suivantes : 
- une base NoSql permet de faire évoluer le schéma de données si le site évolue ou que l'on souhaite compléter la base avec d'autres sources de données
- l'analyseur mis en place permet d'effectuer des recherches rapides sur l'ensemble des champs grace aux processus de tokenisation des données.


__/fastapi__
Folder with fastapi script on port 8000
http://localhost:8000/docs

```console
foo@bar:~/fastapi/$ uvicorn pokedex_api:api --reload & 
``` 