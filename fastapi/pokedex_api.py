from pydantic import BaseModel
from enum import Enum, IntEnum
from typing import Optional, List,Set
import pandas as pd
import json
import random

from fastapi import FastAPI, Depends, Header ,Query, Response, HTTPException, Request
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi.security import HTTPBasic, HTTPBasicCredentials

from elasticsearch import Elasticsearch
es = Elasticsearch(["localhost"], port=9200)
es_collection = "pokedex"

security = HTTPBasic()

api = FastAPI(
    title="POKEDEX API",
    description='This API is used to manage EC database.   __Authentification : reader:demo or admin:4dm1N__',
    version="1.0.0",
    openapi_tags=[
    {
        'name': 'Pokedex',
        'description': 'Functions to interact with EC'
    },
    {
        'name': 'Admininistration',
        'description': 'Functions to manage users'
    },
    {
        'name': 'Service info',
        'description': 'Functions to check service status'
    }
])




#DATA BASES
#DATA BASES
userdb={
    "reader":"demo",
    "admin":"4dm1N"
}
df_user = pd.DataFrame(userdb.items(), columns=['login', 'pass'])

 
 



# global users rights functions
def isUser(login:str, passwd:str):
    """
    Function to validate an existing user
    """
    if not login or not passwd:
        return False
    else:
        dfCurrUser = df_user[(df_user['login']==login) & (df_user['pass']==passwd)]
        if dfCurrUser.empty:
            return False
        else:
            return True


def isAdmin(login:str, passwd:str):
    """
    Function to validate an existing user and check if isAdmin = true
    """
    if not login or not passwd:
        return False
    else:
        dfCurrUser = df_user[(df_user['login']==login) & (df_user['pass']==passwd)]
        if dfCurrUser.empty:
            return False
        else:
            if login=="admin" and passwd=="4dm1N":
                return True
            else:
                return False



# MODELS
class PokeMonModel(BaseModel):
    """
    pokemon model Object : used to validate a pokemon object format
    """
    num: str 
    name_us: str
    name_fr: str

    img_src: Optional[str] = None

    type: str
    espece: str

    taille: float
    poids:float
    
    ethymologie: Optional[str] = None

    ev:int
    talents:str
    tx_capture :int
    bonheur:int
    exp_max:int
    pv:int
    att:int
    defense:int
    att_spe:int
    def_spe:int
    vitesse:int

    immunise:Optional[str] = None
    tres_resistant:Optional[str] = None
    resistant:Optional[str] = None
    vulnerable:Optional[str] = None
    tres_vulnerable:Optional[str] = None

 






#  ROUTES POKEMON :
responses_get_pokedex = {
    200: {"description": "Return JSON formated Pokemon data"},
    404: {"description": "Unregistered user or wrong login / pass values"},
    403: {"description": "User doesn't have the good rights !"},
    500: {"description": "Params missing"},
}

@api.get('/get_pokedex_header', tags=['Pokedex'], responses=responses_get_pokedex, response_model=PokeMonModel)
def get_pokedex_header(Authorization: HTTPBasicCredentials = Depends(security)):  
    """
    API to get first 10 entries from pokedex
    __Header must contain : Authorization Basic username:password__
    """
    response = es.search(
        index=es_collection,
        body= {
            "query": {
                "query_string": {
                    "query": "*"
                }
            },
            "size": 10,
            "from": 0,
            "sort": [
                {
                    "num": {"order":"asc"}
                }
            ]
        }
    )

    # gen json output
    json_response=[]
    for hit in response['hits']['hits']:
        pokemon_json  = { 
                            "_id": hit['_id'], 
                            "num": hit['_source']['num'],
                            "name_us": hit['_source']['name_us'],
                            "name_fr": hit['_source']['name_fr'],

                             

                            "type":  hit['_source']['type'],
                            "espece": hit['_source']['espece'],

                            "taille": hit['_source']['taille'],
                            "poids":hit['_source']['poids'],
                           

                  
                            "talents":hit['_source']['talents'], 
                            "tx_capture" :hit['_source']['tx_capture'],
                            "bonheur":hit['_source']['bonheur'],
                            "exp_max": hit['_source']['exp_max'],
                            "pv": hit['_source']['pv'],
                            "att": hit['_source']['att'],
                            "defense":hit['_source']['def'],
                            "att_spe":hit['_source']['att_spe'],
                            "def_spe":hit['_source']['def_spe'],
                            "vitesse":hit['_source']['vitesse']     
                        } 
        
        if "ev" not in hit['_source']:
            pokemon_json["ev"]= "0"
        else :
            pokemon_json["ev"]= hit['_source']['ev'] 

        if "img_src" in hit['_source']:
            pokemon_json["img_src"]= hit['_source']['img_src'] 
        if "ethymologie" in hit['_source']:
            pokemon_json["ethymologie"]= hit['_source']['ethymologie']                   
        if "immunise" in hit['_source']:
            pokemon_json["immunise"]= hit['_source']['immunise']
        if "tres_resistant" in hit['_source']:
            pokemon_json["tres_resistant"]= hit['_source']['tres_resistant']
        if "resistant" in hit['_source']:
            pokemon_json["resistant"]= hit['_source']['resistant']
        if "vulnerable" in hit['_source']:
            pokemon_json["vulnerable"]= hit['_source']['vulnerable']
        if "tres_vulnerable" in hit['_source']:
            pokemon_json["tres_vulnerable"]= hit['_source']['tres_vulnerable']

 

        json_response.append((pokemon_json))
    return  JSONResponse(json_response)




@api.get('/get_type_count', tags=['Pokedex'], responses=responses_get_pokedex )
def get_type_count(Authorization: HTTPBasicCredentials = Depends(security)):  
    """
    API to get number of Pokemon By Type
    __Header must contain : Authorization Basic username:password__
    """
    response = es.search(
        index=es_collection,
        body= {
                "aggs": {
                    "espece_count": {
                        "terms": {
                            "field": "type",   
                            "size": 10000
                        }
                    }
                }
            }
    )

    return   (response['aggregations']['espece_count']['buckets'] )



 


@api.post('/search_by_name', tags=['Pokedex'], responses=responses_get_pokedex, response_model=PokeMonModel)
def search_name(pokemonName:str, Authorization: HTTPBasicCredentials = Depends(security)):  
    """
    API to search pokemon by Name_FR or Name_Us
    __Header must contain : Authorization Basic username:password__
    """
    response = es.search(
        index=es_collection,
        body= {
            "query": {
                "multi_match": {
                    "query" :"{pokemonName}".format(pokemonName=pokemonName),
                    "fields":["name_us","name_fr"]
                }
            },
            "size": 1000,
            "sort": [
                {
                    "num": {"order":"asc"}
                }
            ]
        }
    )

    # gen json output
    json_response=[]
    for hit in response['hits']['hits']:
        pokemon_json  = { 
                            "_id": hit['_id'], 
                            "num": hit['_source']['num'],
                            "name_us": hit['_source']['name_us'],
                            "name_fr": hit['_source']['name_fr'],

                             

                            "type":  hit['_source']['type'],
                            "espece": hit['_source']['espece'],

                            "taille": hit['_source']['taille'],
                            "poids":hit['_source']['poids'],
                           

                  
                            "talents":hit['_source']['talents'], 
                            "tx_capture" :hit['_source']['tx_capture'],
                            "bonheur":hit['_source']['bonheur'],
                            "exp_max": hit['_source']['exp_max'],
                            "pv": hit['_source']['pv'],
                            "att": hit['_source']['att'],
                            "defense":hit['_source']['def'],
                            "att_spe":hit['_source']['att_spe'],
                            "def_spe":hit['_source']['def_spe'],
                            "vitesse":hit['_source']['vitesse']     
                        } 
        
        if "ev" not in hit['_source']:
            pokemon_json["ev"]= "0"
        else :
            pokemon_json["ev"]= hit['_source']['ev'] 

        if "img_src" in hit['_source']:
            pokemon_json["img_src"]= hit['_source']['img_src'] 
        if "ethymologie" in hit['_source']:
            pokemon_json["ethymologie"]= hit['_source']['ethymologie']                   
        if "immunise" in hit['_source']:
            pokemon_json["immunise"]= hit['_source']['immunise']
        if "tres_resistant" in hit['_source']:
            pokemon_json["tres_resistant"]= hit['_source']['tres_resistant']
        if "resistant" in hit['_source']:
            pokemon_json["resistant"]= hit['_source']['resistant']
        if "vulnerable" in hit['_source']:
            pokemon_json["vulnerable"]= hit['_source']['vulnerable']
        if "tres_vulnerable" in hit['_source']:
            pokemon_json["tres_vulnerable"]= hit['_source']['tres_vulnerable']

 

        json_response.append((pokemon_json))

       

    reponseFullJson ={ 
        "totalResults":len(json_response),
        "list":json_response
    }


    return  JSONResponse(reponseFullJson)



@api.post('/count_by_talents', tags=['Pokedex'], responses=responses_get_pokedex, response_model=PokeMonModel)
def search_name(pokemonName:str, Authorization: HTTPBasicCredentials = Depends(security)):  
    """
    API to search pokemon by Name_FR or Name_Us
    __Header must contain : Authorization Basic username:password__
    """
    response = es.search(
        index=es_collection,
        body= {
            "query": {
                "multi_match": {
                    "query" :"{pokemonName}".format(pokemonName=pokemonName),
                    "fields":["name_us","name_fr"]
                }
            },
            "size": 1000,
            "sort": [
                {
                    "num": {"order":"asc"}
                }
            ]
        }
    )

    # gen json output
    json_response=[]
    for hit in response['hits']['hits']:
        pokemon_json  = { 
                            "_id": hit['_id'], 
                            "num": hit['_source']['num'],
                            "name_us": hit['_source']['name_us'],
                            "name_fr": hit['_source']['name_fr'],

                             

                            "type":  hit['_source']['type'],
                            "espece": hit['_source']['espece'],

                            "taille": hit['_source']['taille'],
                            "poids":hit['_source']['poids'],
                           

                  
                            "talents":hit['_source']['talents'], 
                            "tx_capture" :hit['_source']['tx_capture'],
                            "bonheur":hit['_source']['bonheur'],
                            "exp_max": hit['_source']['exp_max'],
                            "pv": hit['_source']['pv'],
                            "att": hit['_source']['att'],
                            "defense":hit['_source']['def'],
                            "att_spe":hit['_source']['att_spe'],
                            "def_spe":hit['_source']['def_spe'],
                            "vitesse":hit['_source']['vitesse']     
                        } 
        
        if "ev" not in hit['_source']:
            pokemon_json["ev"]= "0"
        else :
            pokemon_json["ev"]= hit['_source']['ev'] 

        if "img_src" in hit['_source']:
            pokemon_json["img_src"]= hit['_source']['img_src'] 
        if "ethymologie" in hit['_source']:
            pokemon_json["ethymologie"]= hit['_source']['ethymologie']                   
        if "immunise" in hit['_source']:
            pokemon_json["immunise"]= hit['_source']['immunise']
        if "tres_resistant" in hit['_source']:
            pokemon_json["tres_resistant"]= hit['_source']['tres_resistant']
        if "resistant" in hit['_source']:
            pokemon_json["resistant"]= hit['_source']['resistant']
        if "vulnerable" in hit['_source']:
            pokemon_json["vulnerable"]= hit['_source']['vulnerable']
        if "tres_vulnerable" in hit['_source']:
            pokemon_json["tres_vulnerable"]= hit['_source']['tres_vulnerable']

 

        json_response.append((pokemon_json))

       

    reponseFullJson ={ 
        "totalResults":len(json_response),
        "list":json_response
    }


    return  JSONResponse(reponseFullJson)




@api.post('/search_by_type', tags=['Pokedex'], responses=responses_get_pokedex, response_model=PokeMonModel)
def search_type(pokemonType:str, Authorization: HTTPBasicCredentials = Depends(security)):  
    """
    API to search pokemon by Type
    __Header must contain : Authorization Basic username:password__
    """
    response = es.search(
        index=es_collection,
        body= {
            "query": {
                "query_string": {
                    "query" :"type:{pokemonType}".format(pokemonType=pokemonType)
                }
            },
            "size": 1000,
            "sort": [
                {
                    "num": {"order":"asc"}
                }
            ]
        }
    )

    # gen json output
    json_response=[]
    for hit in response['hits']['hits']:
        pokemon_json  = { 
                            "_id": hit['_id'], 
                            "num": hit['_source']['num'],
                            "name_us": hit['_source']['name_us'],
                            "name_fr": hit['_source']['name_fr'],

                             

                            "type":  hit['_source']['type'],
                            "espece": hit['_source']['espece'],

                            "taille": hit['_source']['taille'],
                            "poids":hit['_source']['poids'],
                           

                  
                            "talents":hit['_source']['talents'], 
                            "tx_capture" :hit['_source']['tx_capture'],
                            "bonheur":hit['_source']['bonheur'],
                            "exp_max": hit['_source']['exp_max'],
                            "pv": hit['_source']['pv'],
                            "att": hit['_source']['att'],
                            "defense":hit['_source']['def'],
                            "att_spe":hit['_source']['att_spe'],
                            "def_spe":hit['_source']['def_spe'],
                            "vitesse":hit['_source']['vitesse']     
                        } 
        
        if "ev" not in hit['_source']:
            pokemon_json["ev"]= "0"
        else :
            pokemon_json["ev"]= hit['_source']['ev'] 

        if "img_src" in hit['_source']:
            pokemon_json["img_src"]= hit['_source']['img_src'] 
        if "ethymologie" in hit['_source']:
            pokemon_json["ethymologie"]= hit['_source']['ethymologie']                   
        if "immunise" in hit['_source']:
            pokemon_json["immunise"]= hit['_source']['immunise']
        if "tres_resistant" in hit['_source']:
            pokemon_json["tres_resistant"]= hit['_source']['tres_resistant']
        if "resistant" in hit['_source']:
            pokemon_json["resistant"]= hit['_source']['resistant']
        if "vulnerable" in hit['_source']:
            pokemon_json["vulnerable"]= hit['_source']['vulnerable']
        if "tres_vulnerable" in hit['_source']:
            pokemon_json["tres_vulnerable"]= hit['_source']['tres_vulnerable']

 

        json_response.append((pokemon_json))

       

    reponseFullJson ={ 
        "totalResults":len(json_response),
        "list":json_response
    }


    return  JSONResponse(reponseFullJson)







@api.post('/player_cards_hand', tags=['Pokedex'], responses=responses_get_pokedex, response_model=PokeMonModel)
def post_pokedex_cards(NbCard:int, Authorization: HTTPBasicCredentials = Depends(security)):  
    """
    API to get a set of card from pokedex - Player's Hand
    __Header must contain : Authorization Basic username:password__
    """
    response = es.search(
        index=es_collection,
        body= {
            "query": {
                "function_score": {
                    "random_score": {
                        "seed": 10
                    }
                }
            },
            "size": NbCard 
        }
    )

    # gen json output
    json_response=[]
    for hit in response['hits']['hits']:
        pokemon_json  = { 
                            "_id": hit['_id'], 
                            "num": hit['_source']['num'],
                            "name_us": hit['_source']['name_us'],
                            "name_fr": hit['_source']['name_fr'],

                             

                            "type":  hit['_source']['type'],
                            "espece": hit['_source']['espece'],

                            "taille": hit['_source']['taille'],
                            "poids":hit['_source']['poids'],
                           

                    
                            "talents":hit['_source']['talents'], 
                            "tx_capture" :hit['_source']['tx_capture'],
                            "bonheur":hit['_source']['bonheur'],
                            "exp_max": hit['_source']['exp_max'],
                            "pv": hit['_source']['pv'],
                            "att": hit['_source']['att'],
                            "defense":hit['_source']['def'],
                            "att_spe":hit['_source']['att_spe'],
                            "def_spe":hit['_source']['def_spe'],
                            "vitesse":hit['_source']['vitesse']     
                        } 
                        
        if "ev" not in hit['_source']:
            pokemon_json["ev"]= "0"
        else :
            pokemon_json["ev"]= hit['_source']['ev'] 
        if "img_src" in hit['_source']:
            pokemon_json["img_src"]= hit['_source']['img_src'] 
        if "ethymologie" in hit['_source']:
            pokemon_json["ethymologie"]= hit['_source']['ethymologie']                   
        if "immunise" in hit['_source']:
            pokemon_json["immunise"]= hit['_source']['immunise']
        if "tres_resistant" in hit['_source']:
            pokemon_json["tres_resistant"]= hit['_source']['tres_resistant']
        if "resistant" in hit['_source']:
            pokemon_json["resistant"]= hit['_source']['resistant']
        if "vulnerable" in hit['_source']:
            pokemon_json["vulnerable"]= hit['_source']['vulnerable']
        if "tres_vulnerable" in hit['_source']:
            pokemon_json["tres_vulnerable"]= hit['_source']['tres_vulnerable']

        
        json_response.append((pokemon_json))
    
    return  JSONResponse(json_response)





# ADMIN ROUTES
responses_get_user_list = {
    200: {"description": "Return JSON formated user database"},
    404: {"description": "Unregistered user or wrong login / pass values"},
    403: {"description": "User doesn't have the good rights !"},
    500: {"description": "Params missing"},
}

@api.get('/get_users_list', tags=['Admininistration'], responses=responses_get_user_list)
def get_users_list(Authorization: HTTPBasicCredentials = Depends(security)):

    """
    API to get users list (restricted to admin user)

    __Header must contain : Authorization Basic username:password__
    """

    if not Authorization:
        raise HTTPException(status_code=500, detail="Params missing - You need to pass login and password values")
    else:
       
        if not isAdmin(Authorization.username, Authorization.password):
            raise HTTPException(status_code=403, detail="User "+Authorization.username+" doesn't exist or doesn't have the good rights !")
        else :
            return userdb
    


@api.post('/add_pokemon', tags=['Admininistration'], responses=responses_get_user_list)
def add_pokemen(pokemonObj:PokeMonModel, Authorization: HTTPBasicCredentials = Depends(security)):

    """
    API to get users list (restricted to admin user)

    __Header must contain : Authorization Basic username:password__
    """

    if not Authorization:
        raise HTTPException(status_code=500, detail="Params missing - You need to pass login and password values")
    else:
        
         
        if not isAdmin(Authorization.username, Authorization.password):
            raise HTTPException(status_code=403, detail="User "+Authorization.username+" doesn't exist or doesn't have the good rights !")
        else :
            newPokemonCsv = json.dumps(pokemonObj.__dict__ )
            #es.index(index=es_collection,   body=newPokemon)
            pokemonData = list(pokemonObj.__dict__.values())
            csv_line=[]
            for i in range(len(pokemonData)):
                csv_line.append(str(pokemonData[i]))
            response = es.index(index=es_collection,   body={"csv_line":",".join(csv_line)} )

            return response
        

            #print( pokemonData)
  



@api.post('/delete_by_name', tags=['Admininistration'], responses=responses_get_pokedex )
def delete_name(pokemonName:str, Authorization: HTTPBasicCredentials = Depends(security)):  
    """
    API to search pokemon by Name_FR or Name_Us
    __Header must contain : Authorization Basic username:password__
    """
    response =  es.delete_by_query(
                    index=es_collection,  
                    body={
                        "query": {
                            "multi_match": {
                                "query" :"{pokemonName}".format(pokemonName=pokemonName),
                                "fields":["name_us","name_fr"]
                            }
                        }
                    }
                )

     
    return  response







# INFOS
@api.get('/status',tags=['Service info'])
def get_status():
    """
    API to Check if service is running
    """  
    return {
        'status': 1
        }
    


