#!/usr/bin/python

from bs4 import BeautifulSoup
from selenium import webdriver 
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service

import pandas as pd

import argparse

from webdriver_manager.chrome import ChromeDriverManager
from pokemonScrap_class import PokedexScraper 

import time
start_time = time.time()

 
argument_parser = argparse.ArgumentParser()
argument_parser.add_argument('-sl','--scraplimit', type=int, default=0, help='Limit page number to scrap, by default = 0 (Unlimited)')
argument_parser.add_argument('-o','--ofile', type=str, default='pokedex.csv', help='Set csv output file name', required=True)
#argument_parser.print_help()

arguments = argument_parser.parse_args()
scraplimit = arguments.scraplimit
outputfile = arguments.ofile



#config scraping
url2scrap =  "https://eternia.fr/fr/pokedex/liste-pokemon/"   

cust_options = Options()
cust_options.add_experimental_option("detach", True) #force la fenetre a rester ouverte 
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=cust_options)


# open website
driver.get(url2scrap)



def CleanStr(val):
    temp=val.replace("\"", " ")
    temp = temp.replace("\'", " ")
    temp = temp.replace("-", " ")
    temp = temp.replace(",", " ")
    temp = temp.replace(";", " ")
    temp = temp.replace("\r"," ")
    temp = temp.replace("\n"," ")
    temp = temp.replace(u"\u00A0", " ")
    temp= temp.replace('\\','/')
    temp = temp.strip()
    return temp

 
# Save all Pokemon Page link
contentLinkList = driver.find_elements(By.XPATH, "//div[@class='datalist_dex']/table/tbody/tr/td/a")
tabLinks = []
for listLink in contentLinkList:
    tabLinks.append(listLink.get_attribute('href'))


if __name__ == "__main__": 
 
    i = 0
    df = pd.DataFrame(columns=['num',
                            'name_us',
                            'name_fr',
                            'img_src',
                            'type',
                            'espece',
                            'taille',
                            'poids',
                            'ethymologie',
                            'ev',
                            'talents',
                            'tx_capture',
                            'bonheur',
                            'exp_max',
                            'pv',
                            'att',
                            'def',
                            'att_spe',
                            'def_spe',
                            'vitesse',
                            'immunise',
                            'tres_resistant',
                            'resistant',
                            'vulnerable',
                            'tres_vulnerable'                                            
                            ])

    for listLink in tabLinks:
        try:
            currPokemon = PokedexScraper(listLink, driver)
            currPokemon.setPokemonId()
            currPokemon.setPokemonPicture()
            currPokemon.setPokemonInfo()    
            currPokemon.setPokemonEntrainement()
            currPokemon.setPokemonStats()
            currPokemon.setPokemonSpecs()

            currPokemonData = currPokemon.getCurrPokemon() 

            df=df.append( currPokemonData , ignore_index=True)
            print(' ## ',df['num'].iloc[-1],  df['name_fr'].iloc[-1], " --> Saved")

        except Exception as e:
            #print("Error = ",e)
            print(listLink, " --> 404")
            continue 

        i= i+1
        if scraplimit!=0 and scraplimit==i :
            break
        
    df['ev'] = df['ev'].fillna(0) 
    df = df.fillna('') 
    
    df['ethymologie'] =  df['ethymologie'].apply(lambda val: CleanStr(str(val)) )
    df.to_csv('../data/'+outputfile, index=False, header=False)       



    

    print("--- Scraped in %s seconds ---" % round(time.time() - start_time, 2))
    driver.close() # ferme la fenetre
