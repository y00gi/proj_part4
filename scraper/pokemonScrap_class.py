from bs4 import BeautifulSoup
from selenium import webdriver 
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service

class PokedexScraper:
    def __init__(self, url2scrap, driverBrowser) :
        self.url2scrap = url2scrap
        self.browser = driverBrowser
        self.browser.get(url2scrap)

        self.num = ''
        self.name_us = ''
        self.name_fr=''

        self.img_src=''

        self.type=''
        self.espece=''
        self.taille=''
        self.poids=''
        self.ethymologie=''

        self.ev=''
        self.talents=''
        self.tx_capture=''
        self.bonheur=''
        self.exp_max=''

        self.pv=''
        self.att=''
        self.defense=''
        self.att_spe=''
        self.def_spe=''
        self.vitesse=''
        
        self.immunise=''
        self.tres_resistant=''
        self.resistant=''
        self.vulnerable=''
        self.tres_vulnerable=''

    def __str__(self)  :
        return str(self.__dict__)    
 
    def getCurrPokemon(self):
        """
        Fct to save a Pokemon Object into Dict
        """
        currSpecies = {
        'num' :self.num,
        'name_us': self.name_us,
        'name_fr': self.name_fr,
        'img_src': self.img_src,
        'type': self.type,
        'espece': self.espece,
        'taille': self.taille,
        'poids': self.poids,
        'ethymologie':self.ethymologie,
        'ev':self.ev,
        'talents': self.talents,
        'tx_capture': self.tx_capture,
        'bonheur': self.bonheur,
        'exp_max': self.exp_max,

        'pv':self.pv , 
        'att':self.att ,
        'def':self.defense ,
        'att_spe':self.att_spe ,
        'def_spe':self.def_spe ,
        'vitesse':self.vitesse , 

        'immunise': self.immunise, 
        'tres_resistant':self.tres_resistant, 
        'resistant': self.resistant,  
        'vulnerable':self.vulnerable, 
        'tres_vulnerable':self.tres_vulnerable
        }
        return currSpecies


    def setPokemonId(self ):
        """
        Function to set header Pokemon info 
        """
        driver = self.browser
        speciesH1 = driver.find_element(By.XPATH, "//h1[@class='pokedex']").get_attribute('innerHTML')
        soup = BeautifulSoup(speciesH1, 'html.parser')
        
        speciesNumHtml = soup.select(".numdex") 
        speciesNum = speciesNumHtml[0].text.replace('No.','').strip()

        speciesNameUS = soup.select(".lang_us") 
        speciesNameUS = str(speciesNameUS[0].contents[0]).strip()
        for span in soup('span'):
            span.decompose() 

        speciesNameFR = soup.text.replace('•','').strip()

        self.num=speciesNum
        self.name_us = speciesNameUS
        self.name_fr=speciesNameFR
        
    
    def setPokemonPicture(self):
        """
        set Pokemon picture
        """
        driver = self.browser
        img = driver.find_element(By.XPATH, "//div[@class='artwork_off']/img")
        imgSrc = img.get_attribute('src')
        self.img_src = imgSrc

    def setPokemonInfo(self):
        """
        set Pokemon Main Infos
        """
        driver = self.browser
        infoType= driver.find_element(By.XPATH, "//table[@class='tab_infoG']/tbody/tr/th[contains(text(),'Type')]/following-sibling::td")
        infoTypetxt= infoType.text.strip().replace(' ','|')

        infoEspece= driver.find_element(By.XPATH, "//table[@class='tab_infoG']/tbody/tr/th[contains(text(),'Espèce')]/following-sibling::td")
        infoEspecetxt= infoEspece.text.strip() 

        infoTaille= driver.find_element(By.XPATH, "//table[@class='tab_infoG']/tbody/tr/th[contains(text(),'Taille')]/following-sibling::td")
        infoTailletxt= infoTaille.text.strip().split(' ')[0]

        infoPoids= driver.find_element(By.XPATH, "//table[@class='tab_infoG']/tbody/tr/th[contains(text(),'Poids')]/following-sibling::td")
        infoPoidstxt= infoPoids.text.strip().split(' ')[0]

        infoEthymo= driver.find_element(By.XPATH, "//table[@class='tab_infoG']/tbody/tr/th[contains(text(),'Étymologie')]/following-sibling::td")
        infoEthymotxt= infoEthymo.text.strip()

        self.type=infoTypetxt
        self.espece=infoEspecetxt
        self.taille=infoTailletxt
        self.poids=infoPoidstxt
        self.ethymologie=infoEthymotxt


    def setPokemonEntrainement(self):
        """
        set Pokemon Entrainement Infos
        """
        driver = self.browser
        infoEv = driver.find_element(By.XPATH, "//table[@class='tab_infoG']/tbody/tr/th[contains(text(),'EVs')]/following-sibling::td")
        infoEvtxt= infoEv.text.strip().split(' ')[0]

        infoTalentHtml =  driver.find_element(By.XPATH, "//table[@class='tab_infoG']/tbody/tr/th[contains(text(),'Talents')]/following-sibling::td").get_attribute('innerHTML')
        soupTalent = BeautifulSoup(infoTalentHtml, 'html.parser')
        for small in soupTalent('small'):
            small.decompose() 

        infoTalenttxt= str(soupTalent).replace("<br/>", "|")  #.text.strip().replace(' ',',')

        infoTxCaptureHtml =  driver.find_element(By.XPATH, "//table[@class='tab_infoG']/tbody/tr/th[contains(text(),'capture')]/following-sibling::td").get_attribute('innerHTML')
        soupTxCapture = BeautifulSoup(infoTxCaptureHtml, 'html.parser')
        for small in soupTxCapture('small'):
            small.decompose() 
        infoTxCapturettxt= str(soupTxCapture).replace("<br/>", "|")  #.text.strip().replace(' ',',')

        infoBonheur= driver.find_element(By.XPATH, "//table[@class='tab_infoG']/tbody/tr/th[contains(text(),'Bonheur')]/following-sibling::td")
        infoBonheurtxt= infoBonheur.text.strip().split(' ')[0]

        infoExpMax= driver.find_element(By.XPATH, "//table[@class='tab_infoG']/tbody/tr/th[contains(text(),'Expérience')]/following-sibling::td").get_attribute('innerHTML')
        soupExp = BeautifulSoup(infoExpMax, 'html.parser')
        for small in soupExp('small'):
            small.decompose() 

        infoExptxt= str(soupExp).strip().replace(" ", "")

        self.ev=infoEvtxt
        self.talents=infoTalenttxt
        self.tx_capture=infoTxCapturettxt
        self.bonheur=infoBonheurtxt
        self.exp_max=infoExptxt
 
    def setPokemonStats(self):  
        """
        set Pokemon Stats Infos
        """
        driver = self.browser
        infoStatPV= driver.find_element(By.XPATH, "//table[@class='tab_statsB']/tbody/tr/th[contains(text(),'PV')]/following-sibling::td")
        infoStatPvtxt= infoStatPV.text.strip() 

        infoStatAttaque= driver.find_element(By.XPATH, "//table[@class='tab_statsB']/tbody/tr/th[contains(text(),'Attaque')]/following-sibling::td")
        infoStatAttaquetxt= infoStatAttaque.text.strip() 

        infoStatDef= driver.find_element(By.XPATH, "//table[@class='tab_statsB']/tbody/tr/th[contains(text(),'Défense')]/following-sibling::td")
        infoStatDeftxt= infoStatDef.text.strip() 

        infoStatAttSpe= driver.find_element(By.XPATH, "//table[@class='tab_statsB']/tbody/tr/th[contains(text(),'Atq')]/following-sibling::td")
        infoStatAttSpetxt= infoStatAttSpe.text.strip() 

        infoStatDefSpe= driver.find_element(By.XPATH, "//table[@class='tab_statsB']/tbody/tr/th[contains(text(),'Déf Spé')]/following-sibling::td")
        infoStatDefSpetxt= infoStatDefSpe.text.strip() 

        infoStatVitesse= driver.find_element(By.XPATH, "//table[@class='tab_statsB']/tbody/tr/th[contains(text(),'Vitesse')]/following-sibling::td")
        infoStatVitessetxt= infoStatVitesse.text.strip() 

        infoStatTotal= driver.find_element(By.XPATH, "//table[@class='tab_statsB']/tbody/tr/th[contains(text(),'Total')]/following-sibling::td")
        infoStatTotaltxt= infoStatTotal.text.strip().replace(" ", "")

        self.pv=infoStatPvtxt
        self.att=infoStatAttaquetxt
        self.defense=infoStatDeftxt
        self.att_spe=infoStatAttSpetxt
        self.def_spe=infoStatDefSpetxt
        self.vitesse=infoStatVitessetxt

    def setPokemonSpecs(self):
        """
        set Pokemon Specs Infos
        """
        driver = self.browser
        tabImmunise = []
        infoImmunise = driver.find_elements(By.XPATH, "//div[@class='conteneur_faiblesses']/div/h6[contains(text(),'Immunisé')]/following-sibling::div")
        for immu in infoImmunise: 
            tabImmunise.append(immu.text.strip())

        tabImmuniseTxt =   "|".join(tabImmunise)   

        tabTresResistant=[]
        infoTresResistant= driver.find_elements(By.XPATH, "//div[@class='conteneur_faiblesses']/div/h6[contains(text(),'Très résistant')]/following-sibling::div")
        for tresResistant in infoTresResistant: 
            tabTresResistant.append(tresResistant.text.strip())
        
        tabTresResistantTxt = "|".join(tabTresResistant)

        tabResistant = []
        infoResistant= driver.find_elements(By.XPATH, "//div[@class='conteneur_faiblesses']/div/h6[contains(text(),'Résistant')]/following-sibling::div")
        for DivResistant in infoResistant: 
            tabResistant.append(DivResistant.text.strip())

        tabResistantTxt =   "|".join(tabResistant)   

        tabVulnerable = []
        infoVulnerable= driver.find_elements(By.XPATH, "//div[@class='conteneur_faiblesses']/div/h6[contains(text(),'Vulnérable')]/following-sibling::div")
        for div in infoVulnerable: 
            tabVulnerable.append(div.text.strip())

        tabVulnerabletxt =    "|".join(tabVulnerable)  

        tabTresVulnerable = []
        infoTresVulnerable= driver.find_elements(By.XPATH, "//div[@class='conteneur_faiblesses']/div/h6[contains(text(),'Très vulnérable')]/following-sibling::div")
        for div in infoTresVulnerable: 
            tabTresVulnerable.append(div.text.strip())

        tabTresVulnerableTxt = "|".join(tabTresVulnerable)

        self.immunise=tabImmuniseTxt
        self.tres_resistant=tabTresResistantTxt
        self.resistant=tabResistantTxt
        self.vulnerable=tabVulnerabletxt
        self.tres_vulnerable=tabTresVulnerableTxt

