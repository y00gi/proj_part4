
echo "----- LOAD DOCKERS (and wait 10sec) -----"

#ssh -i data_enginering_machine.pem -L 5601:localhost:5601 ubuntu@54.75.82.186
#docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.10.1

#sleep 10




echo "---------------------------------------" 
echo "----- CREATE INGEST CSV PROCESSOR  -----"
echo "---------------------------------------"
curl -X PUT "localhost:9200/_ingest/pipeline/csv_pipeline" -H "Content-Type: application/json" -d @processor.json | jq
sleep 2

echo "---------------------------------------"
echo "----- CREATE INDEX WITH ANALYZER AND MAPPING   -----"
echo "---------------------------------------"
curl -X PUT "localhost:9200/pokedex" -H "Content-Type: application/json" --data-binary "@mapping-analyzer.json" | jq  
sleep 2

echo "---------------------------------------"
echo "----- IMPORT DATA    -----"
echo "---------------------------------------"

sleep 2
 

i=1
sp="/-\|"
echo -n ' '

while read f1
do
   printf "\b${sp:i++%${#sp}:1}"
   f1=$( echo "$f1" | tr -d "\r")
   curl -s -X POST "localhost:9200/pokedex/_doc" -H "Content-Type: application/json" -d "{ \"csv_line\": \"$f1\" }" | jq
   echo " "
done < ../data/pokedex_db.csv

